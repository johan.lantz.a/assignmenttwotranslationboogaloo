export const storageSave = (key, value) =>{
    localStorage.setItem(key, JSON.stringify(value))
}

export const storageRead = (key) => {
    const data = localStorage.getItem(key)
    if (data) {
        return JSON.parse(data)
    }
    return null
}

export const storageDelete = (key) => {
    localStorage.removeItem(key)
}

export const updateStorageHistory = (key, translation) => {
    const data = JSON.parse(localStorage.getItem(key))
    console.log("Data" + data)
    data.translations.push(translation)
    localStorage.setItem(key, JSON.stringify(data))
    return data

}

export const removeStorageHistory = (key) => {
    const data = JSON.parse(localStorage.getItem(key))
    console.log("Data" + data)
    data.translations = []
    localStorage.setItem(key, JSON.stringify(data))
    return data
}