import {  createHeaders } from "./index"

const apiURL = process.env.REACT_APP_API_URL

export const historyAdd = async (user, translation) =>{
    try {
        const response = await fetch(`${apiURL}/${user.id}`,{
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translation]
            })
        })
        if (!response.ok) {
            throw new Error('Could not update translation history')
        }

        const result = await response.json()
        return [ null, result]

    } catch (error) {
        return [error.message, null]
    }
}

export const historyDelete = async (user) =>{
    try {
        const response = await fetch(`${apiURL}/${user.id}`,{
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error('Could not update translation history')
        }

        const result = await response.json()
        return [ null, result]

    } catch (error) {
        return [error.message, null]
    }
}
