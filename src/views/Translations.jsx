import TranslationsForm from "../components/Translations/TranslationsForm"
import withAuth from "../hoc/withAuth"








const Translations = () => {
    return(
        <>
        <h1>Translations</h1>
        <TranslationsForm />
        </>
    )
}

export default withAuth(Translations)