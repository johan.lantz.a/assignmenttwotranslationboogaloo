import './App.css';
import {
  BrowserRouter, 
  Routes, 
  Route
} from 'react-router-dom'
import Login from './views/Login';
import Translations from './views/Translations';
import Profile from './views/Profile';
import Navbar from './components/Navbar/Navbar';

function App() {
  

  return (
    <BrowserRouter>
    <div id='background' className="App">
      <header id='header'><p id='Title'>Lost in Translation</p></header>
      <Navbar/>
      <Routes>
      <Route path="/" element={ <Login/> } />
      <Route path="/Translations" element={ <Translations/> } />
      <Route path="/Profile" element={ <Profile/> } />
      </Routes>
  
    </div>
    </BrowserRouter>
  );
}

export default App;
