import { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { loginUser } from '../../api/user'
import { storageSave } from '../../Utils/storage'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../Context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'
import './Login.css'



const usernameConfig = {
    required: true,
    minLength: 2
}

const LoginForm = () => {

    
    const {register, handleSubmit, formState: {errors}} =  useForm()
    const { user, setUser } = useUser()
    const navigate = useNavigate()


    //local
    const [loading, setLoading] = useState(false)   
    const [apiError, setApiError] = useState(null)

    //side effect
    useEffect(() =>{
        if (user !== null) {
            navigate('/Translations')
        }
    }, [user, navigate ]) 


    // event handler
    const onSubmit = async ({username}) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }

        setLoading(false)

    }

   
    //render function
    const errorMessage = (() => {
        if (!errors.username){
            return null
        }
        if (errors.username.type === 'required') {
            return <span>Username required</span>

        } if (errors.username.type === 'minLength') {
            return <span>Username too short</span>
        }
    })()

    return(
        <>
            <h2>Whats your name</h2>
            <form onSubmit={handleSubmit(onSubmit)}>
            <fieldset>
                <label htmlFor="username"></label>
                <input id='loginInput' type="text" placeholder='username' {... register("username", usernameConfig)} />
                {errorMessage}
                <button id='loginBtn' type='submit' disabled={loading}>Login</button>
            </fieldset>
            

            {loading && <p> Logging in...</p>}
            {apiError && <p>{apiError}</p>}
            </form>
        </>
    )
}
export default LoginForm