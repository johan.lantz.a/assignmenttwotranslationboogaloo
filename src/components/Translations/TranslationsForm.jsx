import { useState } from "react";
import { useForm } from "react-hook-form";
import './Translations.css';
import { useUser } from "../../Context/UserContext";
import { historyAdd } from "../../api/translation";
import { updateStorageHistory } from "../../Utils/storage";
import { STORAGE_KEY_USER } from "../../const/storageKeys";


const translationConfig = {
  required: true,
  minLength: 1
}

const TranslationsForm = () => {
  
  const {user, setUser} = useUser()

  const { register, handleSubmit } = useForm();


  const [ translation, setTranslation ] = useState(null);
 

  const onSubmit = ({ translationInput }) => {
    translate(translationInput)
    if (translationInput.length >= 1 ) {
      setUser(updateStorageHistory(STORAGE_KEY_USER, translationInput))
      historyAdd(user,translationInput)
      
    }
    
  };

  const translate = ( translationInput ) => {
    // console.log(translationInput);

    const mySentence = translationInput;
    const letters = mySentence
      .split("")
      .filter((letter) => /[a-z ]/i.test(letter)); //consider only letters
    const output = [];

    for (let i = 0; i < letters.length; i++) {
      output.push(`img/${letters[i]}.png`);
    }
    setTranslation(output)

  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <fieldset>
        <label htmlFor="translationInput"></label>
        <input placeholder="Enter text for translation" id="translationInput" type="text" {...register("translationInput", translationConfig)}></input>
        <button id="SearchBtn" type="submit">Translate</button>
      </fieldset>
      
      <div id="TranslationsDiv">
        { translation ? translation.map((sign) => (<img src={sign} alt="" />)) : ''}
      </div>
    </form>
  );
};

export default TranslationsForm;
