// simport { Link } from "react-router-dom"
import { historyDelete } from "../../api/translation"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../Context/UserContext"
import { removeStorageHistory, storageDelete } from "../../Utils/storage"
import './Profile.css'




const ProfileActions = () => {


    const {user, setUser} = useUser()

    const handleLogout = () =>{
        if(window.confirm('want to logout?')){
             storageDelete(STORAGE_KEY_USER)
             setUser(null)
        }
    }
    const handleDelete = () =>{
        setUser(removeStorageHistory(STORAGE_KEY_USER))
        historyDelete(user)
    }


    return(
        <>
        
            
            <button id="logoutBtn" onClick={handleLogout} >Logout</button>
            <button id="clearBtn" onClick={handleDelete} >Clear translation history</button>
        
        </>

    )
}

export default ProfileActions