import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"
import './Profile.css'

const ProfileTranslationHistory = ({translations}) => {

    const translationList = translations.map(
        (translation, index) => <ProfileTranslationHistoryItem key={index + '-' + translation } translation={translation}/>)

        
        return(
            <section>
                <h4 id="historyTag" >Translation History</h4>
                <ul id="historyList">
                    {translationList}
                </ul>
            </section>
        )
}

export default ProfileTranslationHistory