const ProfileHeader = ({username}) => {
    return(
        <header>
            <h4 id="greeting" >Hello! {username}</h4>
        </header>
    )
}

export default ProfileHeader