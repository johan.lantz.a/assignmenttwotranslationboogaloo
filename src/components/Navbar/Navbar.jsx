import { NavLink } from "react-router-dom"
import { useUser } from "../../Context/UserContext"
import './Navbar.css'




const Navbar = () => {
    
    
    const {user} = useUser()
    
    

    return(
        <nav>
            
            { user !== null &&
            <ul>
                <li> 
                    <NavLink to="/translations" id="TranslationNav" >Translation </NavLink>
                 
                    <NavLink to="/profile" id="ProfileNav" >{user.username}</NavLink>
                </li>
            </ul>
            }
        </nav>
    )
}

export default Navbar