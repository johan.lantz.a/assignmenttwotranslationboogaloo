import { createContext, useContext, useState } from "react";


//context object -> exposes the value
const TranslationContext = createContext()

export const useTranslator = () => {
    return useContext(TranslationContext) // returns {user, setUser}
}

// provider -> manages state
const TranslationProvider = ({children}) => {
   
    const [ translation, setTransation ] = useState(null)

    const state = {translation, setTransation}

    return(

        <TranslationContext.Provider value={state} >
            {children}
        </TranslationContext.Provider>
    )
}

export default TranslationProvider