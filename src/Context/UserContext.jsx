import { createContext, useContext, useState } from "react";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { storageRead } from "../Utils/storage";

//context object -> exposes the value
const UserContext = createContext()

export const useUser = () => {
    return useContext(UserContext) // returns {user, setUser}
}

// provider -> manages state
const UserProvider = ({children}) => {
   
   const [user, setUser] = useState(storageRead( STORAGE_KEY_USER ))

    const state = {user, setUser}

    return(

        <UserContext.Provider value={state} >
            {children}
        </UserContext.Provider>
    )
}

export default UserProvider