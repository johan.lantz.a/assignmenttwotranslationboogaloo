# Language Translator
##### Sign language translator. React web application.
## Install
#### 1. Fork and Clone repository
#### 2. Install NPM
#### 3. Type in terminal: npm i react-hook-dom
#### 4. Type in terminal: npm i react-hook-form
#### 5. Create a new file and name it ".env". Enter API Key and API URL from Heroku.com:
##### REACT_APP_API_KEY = your key
##### REACT_APP_API_URL = your url
##### (You can find/create your key in settings -> Config vars. Tips: Create key using a guid generator like guidgenerator.com)
##### (Press button "Open App" then "Translations" and copy Url)
#### 6. Doublecheck that .env exists in .gitignore
#### 7. Now it's good to go! Type npm start in terminal to run the website! Good luck 🙂
## Contributors
##### [Johan Lantz (@johan.lantz.a)](@johan.lantz.a)
##### [Pontus Gillson (@d0senb0den)](@d0senb0den)
